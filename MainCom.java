package Java_OOP;
import java.util.ArrayList;
import java.util.Scanner;

public class MainCom {
	static ArrayList<Com> com = new ArrayList<>();
	static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {
		// create list book
		Com com1 = new Com("Com01", 1);
		Com com2 = new Com("Com02", 2);
		Com com3 = new Com("Com03", 3);
		Com com4 = new Com("Com04", 4);
		Com com5 = new Com("Com05", 5);

		// add book to arrayList
		com.add(com1);
		com.add(com2);
		com.add(com3);
		com.add(com4);
		com.add(com5);

		Dele();

	}

	public static void del(int idcom) {
		com.remove(idcom - 1);
		System.out.print(" ID             Name");
		for (Com ComList : com) {
			System.out.print("\n" + ComList);
		}
	}

	public static void Dele() {
		int yourIdcom;
		System.out.print("Input Computer Delete");
		System.out.print(" idcom :");
		boolean Program = false;
		while (Program == false)
			try {
				yourIdcom = Integer.parseInt(input.nextLine());
				del(yourIdcom);
				break;
			} catch (NumberFormatException nfe) {
				System.out.print("idcom Try again: ");
			}
		Program = true;
	}
}

