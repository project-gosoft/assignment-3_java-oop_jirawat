package Java_OOP;

public class Com implements IDCom {

	public String title;
	public int idcom;
	
	public Com(String titleInput, int idcomInput) {
		this.idcom = idcomInput;
		this.title = titleInput;
	}

	@Override
	public String getTitle() {
		// TODO Auto-generated method stub
		return title;
	}

	@Override
	public int getId() {
		// TODO Auto-generated method stub
		return idcom;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return " ID :" + idcom + "  |  " + title;
	}
	
}